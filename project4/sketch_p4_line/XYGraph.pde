abstract class XYGraph extends Graph{
  
  // Scales are in the form of spacing in-between data points and intervals for horizontal and vertical, respectively
  float horizontalScale, verticalScale, maxXValue, maxYValue, yDataScale, xDataScale, yScaleMultiplier, xScaleMultiplier;
  String yColumnTitle, xColumnTitle;
  float defaultAxisIntervalCount = 10.0;
  float yAxisIntervalCount, xAxisIntervalCount;
  float minXValue, minYValue;
  float xDataStart, xDataStop, xPixelStart, xPixelStop, yDataStart, yDataStop, yPixelStart, yPixelStop;
  
  void init(Table table, int xColNumber, int yColNumber){
    setTable(table);
    setScales(xColNumber, yColNumber);
  }
  
  float findMaxColumnValue(String columnName){
    // Get maximum value from column to determine highest point on graph
    float max = 0.0;
    for (TableRow row : table.rows()){
      float columnVal;

      try {
        columnVal = row.getFloat(columnName);
      } catch (NumberFormatException e) {
        columnVal = (float) row.getInt(columnName);
      } catch (Exception e) {
        println("Column values must be ints or floats");
        return -1;
      }

      if (columnVal > max) {
        max = columnVal;
      }

    }
    return max;
  }
  
  float findMinColumnValue(String columnName){
    // Get maximum value from column to determine highest point on graph
    float min = findMaxColumnValue(columnName);
    for (TableRow row : table.rows()){
      float columnVal;

      try {
        columnVal = row.getFloat(columnName);
      } catch (NumberFormatException e) {
        columnVal = (float) row.getInt(columnName);
      } catch (Exception e) {
        println("Column values must be ints or floats");
        return -1;
      }

      if (columnVal < min) {
        min = columnVal;
      }

    }
    return min;
  }

  
  
  void setScales2(int xColNumber, int yColNumber) {
    // Scale data to actual pixels based on intervals
    this.xColumnTitle = table.getRow(0).getColumnTitle(xColNumber);
    this.yColumnTitle = table.getRow(0).getColumnTitle(yColNumber);

    // Find highest X axis value, pick interval count based on highest column value or 10, whichever is lower
    this.maxXValue = findMaxColumnValue(this.xColumnTitle);
    this.minXValue = findMinColumnValue(this.xColumnTitle);
    if (this.maxXValue < defaultAxisIntervalCount)
      this.xAxisIntervalCount = this.maxXValue;
    else
      this.xAxisIntervalCount = defaultAxisIntervalCount;
    // Calculate visual interval scale
    this.horizontalScale = w / this.xAxisIntervalCount;
    // Calculate data scale based on visual intervals
    
    this.xDataScale = this.maxXValue / this.xAxisIntervalCount;
    //this.xDataScale = (this.maxXValue-this.minXValue) / this.xAxisIntervalCount;
    // Scale actual value to width of frame
    this.xScaleMultiplier = w / this.maxXValue;
    //this.xScaleMultiplier = w / (this.maxXValue - this.minXValue);

    // Find highest Y axis value, pick interval count based on highest column value or 10, whichever is lower
    this.maxYValue = findMaxColumnValue(this.yColumnTitle);
    //this.minYValue = findMinColumnValue(this.yColumnTitle);
    if (this.maxYValue < defaultAxisIntervalCount)
      this.yAxisIntervalCount = this.maxYValue;
    else
      this.yAxisIntervalCount = defaultAxisIntervalCount;
    // Calculate visual interval scale
    this.verticalScale = h / this.yAxisIntervalCount;
    // Calculate data scale based on visual intervals
    
    this.yDataScale = this.maxYValue / this.yAxisIntervalCount;    
    //this.yDataScale = (this.maxYValue-this.minYValue) / this.yAxisIntervalCount;
    // Scale actual value to height of frame
    this.yScaleMultiplier = h / this.maxYValue;
    //this.yScaleMultiplier = h / (this.maxYValue - this.minYValue);
  }
  
  
  void setScales(int xColNumber, int yColNumber){
    
    this.xColumnTitle = table.getRow(0).getColumnTitle(xColNumber);
    this.yColumnTitle = table.getRow(0).getColumnTitle(yColNumber);
    
    xPixelStart = x0;
    xPixelStop = x0+w;
    
    //xDataStart = findMinColumnValue(xColumnTitle);
    xDataStart = 0;
    xDataStop = findMaxColumnValue(xColumnTitle);
    
    if (xDataStop < defaultAxisIntervalCount)
      this.xAxisIntervalCount = (int) xDataStop;
    else
      this.xAxisIntervalCount = defaultAxisIntervalCount;

    yPixelStart = y0 + h;
    yPixelStop = y0;    
    
    //yDataStart = findMinColumnValue(yColumnTitle);
    yDataStart = 0;
    yDataStop = findMaxColumnValue(yColumnTitle);
    
    if (yDataStop < defaultAxisIntervalCount)
      this.yAxisIntervalCount = (int) yDataStop; //<>//
    else
      this.yAxisIntervalCount = defaultAxisIntervalCount;
    
  }
  
  void drawVerticalAxis(){
    // Label axis
    textAlign(RIGHT);
    text(yColumnTitle, x0-40, y0+(h/2));
    for (int j = 0; j <= yAxisIntervalCount; j++){

      // Calculate visual interval for ticks/lines
      //float intervalY = j*verticalScale;
      //intervalY = y0+h-intervalY;
      float intervalY = map(j, 0, yAxisIntervalCount, yPixelStart, yPixelStop);
      
      // Label visual interval with actual values scaled to match graph's pixel height
      //int label = (int) (j * yDataScale);
      int label = (int) map(j, 0, yAxisIntervalCount, yDataStart, yDataStop);
      fill(50);
      textAlign(RIGHT);
      text(label, x0-5, intervalY);
      
      // Draw interval lines across graph
      stroke(200);
      strokeWeight(1);
      line(this.x0, intervalY, x0+w, intervalY);
    }
  }
  
  void drawTitle(){
    textAlign(CENTER);
    text(xColumnTitle + " vs. " + yColumnTitle, x0+(w/2), y0-30);
  }
  
  abstract void draw();
}