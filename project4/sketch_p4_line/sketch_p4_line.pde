String filename;
LineGraph lineGraph = null;
Menu menuX = null;
Menu menuY = null;
Table table = null;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

LineGraph setupLineGraph(Table table, int xColumnIndex, int yColumnIndex){
   LineGraph lg = new LineGraph();
   lg.setPosition(200, 100, 400, 400);
   lg.init(table, xColumnIndex, yColumnIndex);
   return lg;
}

Menu setupMenu(Table table, int x0, int y0, int columnIndex) {
   Menu menu = new Menu();
   menu.setPosition(x0, y0, 0, 0);
   menu.setChoices(table, columnIndex);
   return menu;
}

void setup(){
  size(800,600, P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
  int xColumnIndex = 0;
  int yColumnIndex = 1;
  lineGraph = setupLineGraph(table, xColumnIndex, yColumnIndex);
  menuX = setupMenu(table, 650, 100, xColumnIndex);
  menuY = setupMenu(table, 50, 100, yColumnIndex);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  if (lineGraph != null){
    lineGraph = setupLineGraph(table, menuX.getChoice(), menuY.getChoice());
    lineGraph.draw();
  }
  
  if (menuX != null) {
    menuX.drawTitle("X-Axis");
    menuX.draw();
  }
  if (menuY != null) {
    menuY.drawTitle("Y-Axis");
    menuY.draw();
  }

}