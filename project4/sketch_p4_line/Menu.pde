class Menu extends Frame {
  
  ArrayList<String> choices;
  Table table;
  int columnIndex;

  int choiceHeight = 30;
  int maxChoiceLength = 0;
  int charWidth = 8;

  void setChoices(Table table, int columnIndex) {
    this.table = table;
    this.columnIndex = columnIndex;
    this.choices = new ArrayList<String>();
    
    int columns = table.getColumnCount();
    for (int i = 0; i < columns; i++) {
      String columnName = table.getRow(0).getColumnTitle(i);
      this.choices.add(columnName);
    }
    
    for (String choice : choices) {
      if (choice.length() > this.maxChoiceLength) {
        this.maxChoiceLength = choice.length();
      }
    }
    
    // Adjust height and width of frame to make room for choices
    this.w = this.maxChoiceLength;
    this.h = this.choices.size() * choiceHeight;
  }
  
  void drawTitle(String title) {
    fill(0);
    textAlign(LEFT);
    text(title, this.x0, this.y0);
  }
  
  void drawChoices() {
    for (int i = 0; i < this.choices.size(); i++) {
      fill(0, 100, 150);
      textAlign(LEFT);
      int thisHeight = y0+((i+1)*choiceHeight);
      int otherHeight = y0+((i)*choiceHeight);
      text(this.choices.get(i), x0, thisHeight);
      // If mouse over choice, show border
      if (mouseX > x0 && mouseX < x0+this.maxChoiceLength*charWidth && 
      mouseY > otherHeight && mouseY < otherHeight+choiceHeight) {
        stroke(0);
        if (mousePressed){
          this.columnIndex = i;
        }
      }
      else {
        stroke(255);
      }
      noFill();
      rect(x0-5, otherHeight+13, (this.maxChoiceLength * charWidth) + 10, choiceHeight-3);
    }
  }
  
  int getChoice() {
    return this.columnIndex;
  }
  
  void draw(){
    drawChoices();
  }
}