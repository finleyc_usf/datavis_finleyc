class LineGraph extends XYGraph {
  
  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+40);
    for (int i = 0; i <= xAxisIntervalCount; i++) {
      // Get absolute graph coordinates starting from the bottom
      //float bottomY = y0 + h;      
      
      // Calculate visual interval along x-axis
      //float intervalX = i*horizontalScale;
      //intervalX = x0+intervalX;
      float intervalX = map(i, 0, xAxisIntervalCount, xPixelStart, xPixelStop);
      
      // Label visual interval with actual values scaled to match graph's pixel width
      //int label = (int) (i * xDataScale);
      int label = (int) map(i, 0, xAxisIntervalCount, xDataStart, xDataStop);
      fill(50);
      textAlign(CENTER);
      //text(label, intervalX, bottomY+20);
      text(label, intervalX, yPixelStart+20);
    }
  }
  
  void drawTooltip(float posX, float posY, float dataX, float dataY) {
    int rectHeight = 35;
    int charWidth = 8;
    String topString = this.xColumnTitle + ": " + dataX;
    String bottomString = this.yColumnTitle + ": " + dataY;
    int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
    String s = topString + "\n" + bottomString;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    fill(0);
    textAlign(LEFT);
    text(s,posX+5,posY-rectHeight+15, 5);
    translate(0,0,-2);
  }
  
  void drawDataPoints() {
    for (int i = 0; i < table.getRowCount(); i++){
    //for (TableRow row : table.rows()){
      float dataX = table.getRow(i).getFloat(this.xColumnTitle);
      float dataY = table.getRow(i).getFloat(this.yColumnTitle);
      //float posX = x0 + (dataX * xScaleMultiplier);
      float posX = map(dataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
      //float posY = y0+this.h + (dataY * -yScaleMultiplier);
      float posY = map(dataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
      fill(0,0,0);
      noStroke();
      translate(0,0,1);
      ellipse(posX, posY, 5, 5);
      translate(0,0,-1);
      
      if(dist(mouseX,mouseY,posX,posY)<3){
          drawTooltip(posX, posY, dataX, dataY);
      }
      
      // Draw lines from current to next data point (if not at last data point)
      if (i+1 != table.getRowCount()) {
        float nextDataX = table.getRow(i+1).getFloat(this.xColumnTitle);
        float nextDataY = table.getRow(i+1).getFloat(this.yColumnTitle);
        //float nextPosX = x0 + (nextDataX * xScaleMultiplier);
        float nextPosX = map(nextDataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
        //float nextPosY = y0+this.h + (nextDataY * -yScaleMultiplier);
        float nextPosY = map(nextDataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
        stroke(0, 100, 150);
        line(posX, posY, nextPosX, nextPosY);
      }
      fill(50);
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
  
}