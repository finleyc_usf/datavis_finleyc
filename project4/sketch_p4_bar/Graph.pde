abstract class Graph extends Frame{
  
  Table table;
  
  void setTable(Table table){
    this.table = table;
  }
  
  abstract void draw();
}