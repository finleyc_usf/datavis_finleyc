String filename;
BarGraph barGraph = null;
Menu menuX = null;
Menu menuY = null;
Table table = null;

void fileSelected(File selection) {
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  Table table = loadTable(filename, "header");
  return table;
}

BarGraph setupBarGraph(Table table, int xColumnIndex, int yColumnIndex){
     BarGraph bg = new BarGraph();
     bg.setPosition(200, 100, 400, 400);
     bg.init(table, xColumnIndex, yColumnIndex);
     return bg;
}

Menu setupMenu(Table table, int x0, int y0, int columnIndex) {
   Menu menu = new Menu();
   menu.setPosition(x0, y0, 0, 0);
   menu.setChoices(table, columnIndex);
   return menu;
}


void setup(){
  size(800,600,P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
  int xColumnIndex = 0;
  int yColumnIndex = 1;
  barGraph = setupBarGraph(table, xColumnIndex, yColumnIndex);
  menuX = setupMenu(table, 650, 100, xColumnIndex);
  menuY = setupMenu(table, 50, 100, yColumnIndex);
}

void waitForFilename(){
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  if (barGraph != null){
    barGraph.init(table, menuX.getChoice(), menuY.getChoice());
    barGraph.draw();
  }
  if (menuX != null) {
    menuX.drawTitle("X-Axis");
    menuX.draw();
  }
  if (menuY != null) {
    menuY.drawTitle("Y-Axis");
    menuY.draw();
  }
}