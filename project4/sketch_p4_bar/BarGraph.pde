class BarGraph extends XYGraph {
  
  int barWidth = 5;
  
  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+40);
    for (int i = 0; i <= xAxisIntervalCount; i++) {
      // Get absolute graph coordinates starting from the bottom
      float bottomY = y0 + h;      
      
      // Calculate visual interval along x-axis
      float intervalX = i*horizontalScale;
      intervalX = x0+intervalX;
      
      // Label visual interval with actual values scaled to match graph's pixel width
      int label = (int) (i * xDataScale);
      fill(50);
      textAlign(CENTER);
      text(label, intervalX, bottomY+20);
    }
  }
  
  void drawTooltip(float posX, float posY, float dataX, float dataY) {
    int rectHeight = 35;
    int charWidth = 8;
    String topString = this.xColumnTitle + ": " + dataX;
    String bottomString = this.yColumnTitle + ": " + dataY;
    int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
    String s = topString + "\n" + bottomString;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    fill(0);
    textAlign(LEFT);
    text(s,posX+5,posY-rectHeight+15, 5);
    translate(0,0,-2);
  }
  
  void drawDataPoints() {
    for (TableRow row : table.rows()){
      float dataX = row.getFloat(this.xColumnTitle);
      float dataY = row.getFloat(this.yColumnTitle);
      float posX = x0 + (dataX * xScaleMultiplier);
      float rectHeight = dataY * yScaleMultiplier;
      fill(0,100,150);
      noStroke();
      translate(0,0,1);
      rect(posX-barWidth, y0+h, barWidth, -rectHeight);
      translate(0,0,-1);

      if (mouseX > posX-barWidth && mouseX < posX && 
      mouseY < this.y0+h && mouseY > y0+h-rectHeight) {
        drawTooltip(mouseX, mouseY, dataX, dataY);
      }
    }
  }
   
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
}