abstract class Frame {
  
  int x0,y0,w,h;
  
  void setPosition( int x0, int y0, int w, int h ){
    this.x0 = x0;
    this.y0 = y0;
    this.w = w;
    this.h = h;
  }
  
  void drawFrame(){
    stroke(0);
    strokeWeight(1);
    noFill();
    background(255);
    rect(this.x0, this.y0, this.w, this.h);
  }
  
  abstract void draw();
}