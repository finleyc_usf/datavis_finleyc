class ScatterPlot extends XYGraph {

  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+40);
    for (int i = 0; i <= xAxisIntervalCount; i++) {
      // Get absolute graph coordinates starting from the bottom
      float bottomY = y0 + h;      
      
      // Calculate visual interval along x-axis
      float intervalX = i*horizontalScale;
      intervalX = x0+intervalX;
      
      // Label visual interval with actual values scaled to match graph's pixel width
      int label = (int) (i * xDataScale);
      fill(50);
      textAlign(CENTER);
      text(label, intervalX, bottomY+20);
    }
  }
  
  void drawTooltip(float posX, float posY, float dataX, float dataY) {
    // Draw rectangle containing key value pairs for graph data 
    int rectHeight = 35;
    int charWidth = 8;
    String topString = this.xColumnTitle + ": " + dataX;
    String bottomString = this.yColumnTitle + ": " + dataY;
    int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
    String s = topString + "\n" + bottomString;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    fill(0);
    textAlign(LEFT);
    text(s,posX+5,posY-rectHeight+15, 5);
    translate(0,0,-2);
  }
  
  void drawDataPoints() {
    for (TableRow row : table.rows()){
      float dataX = row.getFloat(this.xColumnTitle);
      float dataY = row.getFloat(this.yColumnTitle);
      float posX = x0 + (dataX * xScaleMultiplier);
      float posY = y0+this.h + (dataY * -yScaleMultiplier);
      fill(0,0,0);
      noStroke();
      translate(0,0,1);
      ellipse(posX, posY, 5, 5);
      translate(0,0,-1);

      if(dist(mouseX,mouseY,posX,posY)<3){
        // If user hovering over/close to data point, show tooltip
        drawTooltip(posX, posY, dataX, dataY);
      }
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
}