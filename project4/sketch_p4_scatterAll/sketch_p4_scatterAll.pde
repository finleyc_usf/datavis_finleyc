String filename;
Table table;
int xDetailColumnIndex = 0;
int yDetailColumnIndex = 0;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

ScatterPlot setupScatterPlot(Table table, int xColumnIndex, int yColumnIndex){
     ScatterPlot sp = new ScatterPlot();
     // Position scatter plot based off of column indexes being used, also add some 50px paddings
     sp.setPosition((yColumnIndex*200 % 800)+(50*(yColumnIndex+2)), (xColumnIndex*200)+(50*(xColumnIndex+1)), 100, 100);
     sp.init(table, xColumnIndex, yColumnIndex);
     return sp;
}

ScatterPlotDetail setupScatterPlotBig(Table table, int xColumnIndex, int yColumnIndex){
   ScatterPlotDetail sp = new ScatterPlotDetail();
   sp.setPosition(1150, 300, 400, 400);
   sp.init(table, xColumnIndex, yColumnIndex);
   return sp;
}

void setup(){
  size(1600,1000, P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  // Go through each combination of variables and draw their scatter plot
  for (int i = 0; i < table.getColumnCount(); i++) {
    for (int j = 0; j < table.getColumnCount(); j++) {
      ScatterPlot scatterPlot = setupScatterPlot(table, i, j);
       if (scatterPlot != null){
         scatterPlot.draw();
         if (scatterPlot.picked) {
           // Smaller plot clicked on, update global columns/axes for use in detailed plot view
           xDetailColumnIndex = scatterPlot.pickedXColumnIndex;
           yDetailColumnIndex = scatterPlot.pickedYColumnIndex;
         }
       }
    }
  }
  // Create enlarged scatter plot, if smaller plot picked, this graph's axes will change
  ScatterPlotDetail scatterPlotBig = setupScatterPlotBig(table, xDetailColumnIndex, yDetailColumnIndex);
  if (scatterPlotBig != null){
    scatterPlotBig.draw();
  }
}