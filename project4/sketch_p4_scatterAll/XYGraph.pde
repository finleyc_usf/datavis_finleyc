abstract class XYGraph extends Graph{
  
  // Scales are in the form of spacing in-between data points and intervals for horizontal and vertical, respectively
  float horizontalScale, verticalScale, maxXValue, maxYValue, yDataScale, xDataScale, yAxisIntervalCount, xAxisIntervalCount, yScaleMultiplier, xScaleMultiplier;
  String yColumnTitle, xColumnTitle;
  float defaultAxisIntervalCount = 3.0;
  int pickedXColumnIndex, pickedYColumnIndex;
  
  void init(Table table, int xColNumber, int yColNumber){
    setTable(table);
    pickedXColumnIndex = xColNumber;
    pickedYColumnIndex = yColNumber;
    setScales(xColNumber, yColNumber);
  }
  
  float findMaxColumnValue(String columnName){
    // Get maximum value from column to determine highest point on graph
    float max = 0.0;
    for (TableRow row : table.rows()){
      float columnVal;

      try {
        columnVal = row.getFloat(columnName);
      } catch (NumberFormatException e) {
        columnVal = (float) row.getInt(columnName);
      } catch (Exception e) {
        println("Column values must be ints or floats");
        return -1;
      }

      if (columnVal > max) {
        max = columnVal;
      }

    }
    return max;
  }

  void setScales(int xColNumber, int yColNumber) {
    // Scale data to actual pixels based on intervals
    this.xColumnTitle = table.getRow(0).getColumnTitle(xColNumber);
    this.yColumnTitle = table.getRow(0).getColumnTitle(yColNumber);

    // Find highest X axis value, pick interval count based on highest column value or 10, whichever is lower
    this.maxXValue = findMaxColumnValue(this.xColumnTitle);
    if (this.maxXValue < defaultAxisIntervalCount)
      this.xAxisIntervalCount = this.maxXValue;
    else
      this.xAxisIntervalCount = defaultAxisIntervalCount;
    // Calculate visual interval scale
    this.horizontalScale = w / this.xAxisIntervalCount;
    // Calculate data scale based on visual intervals
    this.xDataScale = this.maxXValue / this.xAxisIntervalCount;
    // Scale actual value to width of frame
    // Multiply data point by this to get actual pixel position
    this.xScaleMultiplier = w / maxXValue;

    // Find highest Y axis value, pick interval count based on highest column value or 10, whichever is lower
    this.maxYValue = findMaxColumnValue(this.yColumnTitle);
    if (this.maxYValue < defaultAxisIntervalCount)
      this.yAxisIntervalCount = this.maxYValue;
    else
      this.yAxisIntervalCount = defaultAxisIntervalCount;
    // Calculate visual interval scale
    this.verticalScale = h / this.yAxisIntervalCount;
    // Calculate data scale based on visual intervals
    this.yDataScale = this.maxYValue / this.yAxisIntervalCount;
    // Scale actual value to height of frame
    this.yScaleMultiplier = h / this.maxYValue;
  }
  
  void drawVerticalAxis(){
    // Label axis
    fill(0);
    textAlign(RIGHT);
    text(yColumnTitle, x0-40, y0+(h/2));
    for (int j = 0; j <= yAxisIntervalCount; j++){

      // Calculate visual interval for ticks/lines
      float intervalY = j*verticalScale;
      intervalY = y0+h-intervalY;
      
      // Label visual interval with actual values scaled to match graph's pixel height
      int label = (int) (j * yDataScale);
      fill(50);
      textAlign(RIGHT);
      text(label, x0-5, intervalY);
      
      // Draw interval lines across graph
      stroke(200);
      strokeWeight(1);
      line(this.x0, intervalY, x0+w, intervalY);
    }
  }
  
  void drawTitle(){
    textAlign(CENTER);
    fill(0);
    text(xColumnTitle + " vs. " + yColumnTitle, x0+(w/2), y0-30);
  }
  
  abstract void draw();
}