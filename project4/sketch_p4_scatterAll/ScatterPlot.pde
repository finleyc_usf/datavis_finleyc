class ScatterPlot extends XYGraph {
  
  boolean picked = false;

  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+40);
    for (int i = 0; i <= xAxisIntervalCount; i++) {
      // Get absolute graph coordinates starting from the bottom
      float bottomY = y0 + h;      
      
      // Calculate visual interval along x-axis
      float intervalX = i*horizontalScale;
      intervalX = x0+intervalX;
      
      // Label visual interval with actual values scaled to match graph's pixel width
      int label = (int) (i * xDataScale);
      fill(50);
      textAlign(CENTER);
      text(label, intervalX, bottomY+20);
    }
  }
  
  void drawDataPoints() {
    for (TableRow row : table.rows()){
      float rowX = x0 + (row.getFloat(xColumnTitle) * xScaleMultiplier);
      float rowY = y0+h + (row.getFloat(yColumnTitle) * -yScaleMultiplier);
      fill(0,0,0);
      ellipse(rowX, rowY, 5, 5);
    }
  }
  
  void drawFrame(){
    // Redefine this function for smaller plots to show border rectangle when moused-over
    stroke(0);
    strokeWeight(1);
    noFill();
    rect(this.x0, this.y0, this.w, this.h);
    if (mouseX > x0 && mouseX < x0+w && 
      mouseY < this.y0+h && mouseY > y0) {
      stroke(0);
      if (mousePressed) {
        this.picked = true;
      }
    }
    else {
      noStroke();
    }
    noFill();
    rect(this.x0-30, this.y0-30, this.w+60, this.h+60);
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
  
}