abstract class Frame {
  
  int x0,y0,w,h;
  
  void setPosition( int x0, int y0, int w, int h ){
    this.x0 = x0;
    this.y0 = y0;
    this.w = w;
    this.h = h;
  }
  
  void drawFrame(){
    stroke(0); //<>//
    strokeWeight(1);
    noFill();
    rect(x0, y0, w, h);
  }
  
  abstract void draw();
}