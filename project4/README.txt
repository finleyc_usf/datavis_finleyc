README

Since these sketches use a 3D renderer, the file dialog that opens up when you start the sketch may be hidden behind the main window.
Either alt-tab to find it or move the main window out of the way.
Also, the main window sometimes isn't focusable (at least in Windows). Solution is to exit out of Processing all-together and restart by clicking on the sketch file agin. 