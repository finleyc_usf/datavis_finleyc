class ScatterPlot extends XYGraph {

  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+40);
    for (int i = 0; i <= xAxisIntervalCount; i++) {
      // Get absolute graph coordinates starting from the bottom
      float bottomY = y0 + h;      
      
      // Calculate visual interval along x-axis
      float intervalX = i*horizontalScale;
      intervalX = x0+intervalX;
      
      // Label visual interval with actual values scaled to match graph's pixel width
      int label = (int) (i * xDataScale);
      fill(50);
      textAlign(CENTER);
      text(label, intervalX, bottomY+20);
    }
  }
  
  void drawDataPoints() {
    for (TableRow row : table.rows()){
      float rowX = x0 + (row.getFloat(xColumnTitle) * xScaleMultiplier);
      float rowY = y0+h + (row.getFloat(yColumnTitle) * -yScaleMultiplier);
      fill(0,0,0);
      ellipse(rowX, rowY, 5, 5);
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
  
}