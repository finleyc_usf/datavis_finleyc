String filename;
ScatterPlot scatterPlot = null;
Table table;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

ScatterPlot setupScatterPlot(Table table, int xColumnIndex, int yColumnIndex){
     ScatterPlot sp = new ScatterPlot();
     // Position scatter plot based off of column indexes being used, also add some 50px paddings
     sp.setPosition((yColumnIndex*200 % 800)+(50*(yColumnIndex+2)), (xColumnIndex*200)+(50*(xColumnIndex+1)), 150, 150);
     sp.init(table, xColumnIndex, yColumnIndex);
     return sp;
}

void setup(){
  size(1100,1000);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  // Go through each combination of variables and draw their scatter plot
  for (int i = 0; i < table.getColumnCount(); i++) {
    for (int j = 0; j < table.getColumnCount(); j++) {
      ScatterPlot scatterPlot = setupScatterPlot(table, i, j);
       if (scatterPlot != null){
         scatterPlot.draw();
       }
    }
  }
}