String filename;
ScatterPlot scatterPlot = null;


void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

ScatterPlot setupScatterPlot(Table table){
     ScatterPlot sp = new ScatterPlot();
     sp.setPosition(200, 100, 400, 400);
     int xColumnIndex = 0;
     int yColumnIndex = 1;
     sp.init(table, xColumnIndex, yColumnIndex);
     return sp;
}

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  Table table = processCSV(filename);
  scatterPlot = setupScatterPlot(table);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  if (scatterPlot != null){
    scatterPlot.draw();
  }

}