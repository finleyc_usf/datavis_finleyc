class LineGraph extends XYGraph {

  void drawHorizontalAxis(){
    textAlign(LEFT);
    text(xColumnTitle, (x0+w)/2, (y0+h)+40);
    for (int i = 0; i < table.getRowCount(); i++) {
      TableRow row = table.getRow(i);
      String year = row.getString(xColumnTitle);
      float val1 = row.getInt(yColumnTitle) * -1 * scaleMultiplier;
      
      // Get absolute graph coordinates starting from the bottom
      float bottomX = x0 + i * (spacingWidth + barWidth);
      float bottomY = y0 + h;
      
      // Draw line nodes
      strokeWeight(2);
      stroke(255,0,0);
      fill(255,0,0);
      ellipse(bottomX+spacingWidth, bottomY+val1, 5, 5);
      
      // Draw lines from current to next data point (if not at last data point)
      if (i+1 != table.getRowCount()) {
        float nextX = x0 + (i+1) * (spacingWidth + barWidth);
        float nextVal = table.getRow(i+1).getInt(yColumnTitle) *-1 * scaleMultiplier;
        line(bottomX+spacingWidth, bottomY+val1, nextX+spacingWidth, bottomY+nextVal);
      }
      fill(50);
      
      // Label axis
      textAlign(LEFT);
      text(year, bottomX, bottomY + spacingWidth);
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
  }
  
}