String filename;
LineGraph lineGraph = null;


void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

LineGraph setupLineGraph(Table table){
     LineGraph lg = new LineGraph();
     lg.setPosition(100, 100, 400, 400);
     lg.init(table);
     return lg;
}

void setup(){
  size(800,800);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  Table table = processCSV(filename);
  lineGraph = setupLineGraph(table);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  if (lineGraph != null){
    lineGraph.draw();
  }

}