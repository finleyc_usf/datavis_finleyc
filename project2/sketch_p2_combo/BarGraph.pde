class BarGraph extends XYGraph {
  
  void drawHorizontalAxis(){
    // Label axis
    textAlign(LEFT);
    text(xColumnTitle, (x0+w)/2, (y0+h)+40);
    for (int i = 0; i < table.getRowCount(); i++) {
      TableRow row = table.getRow(i);
      String year = row.getString(xColumnTitle);
      float val1 = row.getInt(yColumnTitle) * -1 * scaleMultiplier;
      
      // Get absolute graph coordinates starting from the bottom
      float bottomX = x0 + i * (spacingWidth + barWidth);
      float bottomY = y0 + h;
      
      // Draw bar
      fill(0, 100, 255);
      rect(bottomX, bottomY, barWidth, val1);
      
      // Draw axis data point label
      fill(50);
      textAlign(LEFT);
      text(year, bottomX, bottomY + spacingWidth);
    }
  }
   
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
  }
}