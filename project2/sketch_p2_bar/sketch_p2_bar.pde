String filename;
BarGraph barGraph = null;

void fileSelected(File selection) {
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  Table table = loadTable(filename, "header");
  return table;
}

BarGraph setupBarGraph(Table table){
     BarGraph bg = new BarGraph();
     bg.setPosition(100, 100, 400, 400);
     bg.init(table);
     return bg;
}

void setup(){
  size(600,600);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  Table table = processCSV(filename);
  barGraph = setupBarGraph(table);
}

void waitForFilename(){
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  if (barGraph != null){
    barGraph.draw();
  }
}