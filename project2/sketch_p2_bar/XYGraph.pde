abstract class XYGraph extends Graph{
  
  // Scales are in the form of spacing in-between data points and intervals for horizontal and vertical, respectively
  float horizontalScale, verticalScale, maxColumnValue, dataScale, yAxisIntervalCount, scaleMultiplier;
  String yColumnTitle, xColumnTitle;
  float barWidth, spacingWidth;
  
  void init(Table table){
    setTable(table);
    setScales();
    setBarWidths();
  }

  void setBarWidths() {
    // Determine distance between data points (line nodes on line chart or bars on bar graph)
    this.barWidth = (0.66) * this.horizontalScale;
    this.spacingWidth = this.horizontalScale - barWidth;
  }
  
  int findMaxColumnValue(String columnName){
    // Get maximum value from column to determine highest point on graph
    int max = 0;
    for (TableRow row : table.rows()){
      int columnVal;

      try {
        columnVal = row.getInt(columnName);
      } catch (NumberFormatException e) {
        columnVal = (int) row.getFloat(columnName);
      } catch (Exception e) {
        println("Column values must be ints or floats");
        return -1;
      }

      if (columnVal > max) {
        max = columnVal;
      }

    }
    return max;
  }

  void setScales() {
    // Scale data to available pixels
    this.horizontalScale = w / table.getRowCount();
    
    this.yAxisIntervalCount = 10;
    
    this.xColumnTitle = table.getRow(0).getColumnTitle(0);
    this.yColumnTitle = table.getRow(0).getColumnTitle(2);
    this.maxColumnValue = findMaxColumnValue(this.yColumnTitle);
    
    // Get scales for y axis intervals and the corresponding scaled data value
    this.verticalScale = h / this.yAxisIntervalCount;
    this.dataScale = this.maxColumnValue / this.yAxisIntervalCount;
    
    // Scale actual value to height of frame
    this.scaleMultiplier = h / this.maxColumnValue;
  }
  
  void drawVerticalAxis(){
    for (int j = 0; j <= yAxisIntervalCount; j++){
      // Map actual data values to allotted pixels
      float intervalY = j*verticalScale;
      intervalY = y0+h-intervalY;
      
      // Actual value scaled to match graph's pixel height
      int label = (int) (j * dataScale);
      fill(50);
      textAlign(RIGHT);
      text(label, x0-5, intervalY);
      
      stroke(200);
      strokeWeight(1);
      line(this.x0, intervalY, x0+w, intervalY);
    }
    // Label axis
    textAlign(RIGHT);
    text(yColumnTitle, x0-30, y0+(h/2));
  }
  
  void drawTitle(){
    textAlign(CENTER);
    text(yColumnTitle + " Over Time", x0+(w/2), y0-30);
  }
  
  abstract void draw();
}