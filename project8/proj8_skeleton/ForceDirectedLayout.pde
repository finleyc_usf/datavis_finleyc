// most modification should occur in this file


class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 20.0f;   // update this value
  float SPRING_SCALE   = 0.00075f; // update this value
  float REPULSE_SCALE  = 0.00001f;  // update this value
  int radius = 15;

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;
  

  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    
    float xDistance = v0.pos.x - v1.pos.x;
    float yDistance = v0.pos.y - v1.pos.y;
    float d = sqrt(pow((xDistance), 2) + pow((yDistance), 2));
    float m1 = v0.mass;
    float m2 = v1.mass;
    float xNormal = xDistance / d;
    float yNormal = yDistance / d;
    float force = (REPULSE_SCALE * m1 * m2) / pow(d, 2);
    
    v0.addForce(force * xNormal, force * yNormal);
    v1.addForce(force * xNormal, force * yNormal);
  }

  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    float xDistance = edge.v0.pos.x - edge.v1.pos.x;
    float yDistance = edge.v0.pos.y - edge.v1.pos.y;
    float d = sqrt(pow((xDistance), 2) + pow((yDistance), 2));
    float xNormal = xDistance / d;
    float yNormal = yDistance / d;
    float L = RESTING_LENGTH;
    
    float force = SPRING_SCALE * max(0, d-L);
    
    edge.v0.addForce(force * xNormal, force * yNormal);
    edge.v1.addForce(force * xNormal, force * yNormal);
  }

  void draw() {
    update(); // don't modify this line
    
    // TODO: ADD CODE TO DRAW THE GRAPH
    
    for (GraphEdge edge : edges) {
      stroke(150, 150, 150);
      strokeWeight(edge.weight * (1/10) + 1);
      line(edge.v0.pos.x, edge.v0.pos.y, edge.v1.pos.x, edge.v1.pos.y);
      strokeWeight(1);
    }
    
    for (GraphVertex vert : verts) {
      colorMode(HSB, 100);
      fill(vert.group*10, 100, 100); //<>//
      stroke(0,0,255);
      strokeWeight(2);
      ellipse(vert.pos.x, vert.pos.y, radius, radius);
      colorMode(RGB, 255);
    }    
  }


  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE

  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE

  }

  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}