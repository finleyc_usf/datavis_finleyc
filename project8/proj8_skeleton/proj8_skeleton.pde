

ForceDirectedLayout myFrame = null;
JSONObject json;

void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
} 

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();


    // TODO: PUT CODE IN TO LOAD THE GRAPH
    
    json = loadJSONObject(selection.getAbsolutePath());
    JSONArray nodes = json.getJSONArray("nodes");
    for (int i = 0; i < nodes.size(); i++) {
      JSONObject node =  nodes.getJSONObject(i);
      String id = node.getString("id");
      int group = node.getInt("group");
      float x = random(400,450);
      float y = random(400,450);
      GraphVertex v = new GraphVertex(id, group, x, y);
      verts.add(v);
    }
    
    JSONArray links = json.getJSONArray("links");
    for (int i = 0; i < links.size(); i++) {
      JSONObject link =  links.getJSONObject(i);
      String u = link.getString("source");
      String v = link.getString("target");
      int weight = link.getInt("value");
      GraphVertex source = null;
      GraphVertex target = null;
      for (GraphVertex vert : verts) {
        if (vert.id.equals(u))
          source = vert;
        else if (vert.id.equals(v)) {
          target = vert;
          
        }
      }
      GraphEdge e = new GraphEdge(source, target, weight);
      edges.add(e);
    }
     myFrame = new ForceDirectedLayout(verts, edges);
  }
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

/*
void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}
*/

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  //void mousePressed() { }
  //void mouseReleased() { }
  
  float area() {
    return w*h;
  }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}