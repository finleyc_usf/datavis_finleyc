class BarGraph extends XYGraph {
  
  int barWidth = 2;
  
  void drawDataPoints() {
    for (int i=0; i < table.getRowCount(); i++) {
      TableRow row = table.getRow(i);
      float dataX = row.getFloat(this.xColumnTitle);
      float dataY = row.getFloat(this.yColumnTitle);
      
      float posX = map(dataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
      float rectHeight = map(dataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
      
      fill(0,100,150);
      
      if (currentRowIndex == i){
        // If on synced data point, enlarge bar size and make red
        barWidth = 4;
        fill(255,0,0);
      }

      // Hover
      if (mouseX > posX-barWidth && mouseX < posX && 
      mouseY < this.y0+h && mouseY > y0+h-rectHeight) {
        drawTooltip(mouseX, mouseY, dataX, dataY);
        // Update synced data point for global use
        updateCurrentRowIndex(i);
        fill(255,0,0);
        barWidth = 3;
      }

      noStroke();
      translate(0,0,1);
      rect(posX, y0+h, barWidth, -(y0+h-rectHeight));
      translate(0,0,-1);
      fill(0);
      barWidth = 2;
      
    }
  }
   
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
}