String filename;
BarGraph barGraph = null;
LineGraph lineGraph = null;
ScatterPlot scatterPlot = null;
ScatterPlot scatterPlotMatrix = null;
ParallelGraph pGraph = null;
Table table = null;
int currentRowIndex = 0;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

BarGraph setupBarGraph(Table table){
     BarGraph bg = new BarGraph();
     bg.setPosition(70, 50, 200, 200);
     bg.init(table, currentRowIndex);
     return bg;
}

LineGraph setupLineGraph(Table table){
   LineGraph lg = new LineGraph();
   lg.setPosition(360, 50, 200, 200);
   lg.init(table, currentRowIndex);
   return lg;
}

ScatterPlot setupScatterPlot(Table table){
   ScatterPlot sp = new ScatterPlot();
   sp.setPosition(650, 50, 200, 200);
   sp.init(table, currentRowIndex);
   return sp;
}

ScatterPlot setupScatterPlotMatrix(Table table, int plotIndex, int xColumnIndex, int yColumnIndex){
     ScatterPlot sp = new ScatterPlot();
     // Position scatter plots based off of column indexes being used, fit to # of columns //<>//
     int columnCount = 8;
     int currentMatrixRow = plotIndex / columnCount;
     int currentMatrixRowPixelOffset = (currentMatrixRow)*160 + 320;
     int currentMatrixColumn = plotIndex % columnCount;
     int currentMatrixColumnPixelOffset = (currentMatrixColumn)*160 + 100;
     sp.setPosition(currentMatrixColumnPixelOffset, currentMatrixRowPixelOffset, 100, 100);
     sp.initMatrix(table, xColumnIndex, yColumnIndex, currentRowIndex);
     return sp;
}

ParallelGraph setupParallelGraph(Table table){
   ParallelGraph pg = new ParallelGraph();
   pg.setPosition(920, 50, 400, 200);
   pg.init(table, currentRowIndex);
   return pg;
}

void setup(){
  // Couldn't fit everything neatly into 1200x800
  size(1600,1000,P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
  barGraph = setupBarGraph(table);
  lineGraph = setupLineGraph(table);
  scatterPlot = setupScatterPlot(table);
  pGraph = setupParallelGraph(table);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void drawTooltip() {
    // Tooltip, but for synced data point, off to side of all graphs
    int rectHeight = 20 * table.getColumnCount();
    int charWidth = 7;
    String text = "";
    int rectLength = 0;
    TableRow currentRow = table.getRow(currentRowIndex);
    for (int i = 0; i < table.getColumnCount(); i++){
      String label = currentRow.getColumnTitle(i);
      String data = currentRow.getString(i);
      String line = label + ": " + data + "\n";
      if ((line.length() * charWidth) > rectLength) {
        rectLength = line.length() * charWidth;
      }
      text += line;
    }
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(1370, 50, rectLength, rectHeight);
    fill(0);
    textAlign(LEFT);
    text(text, 1375, 65);
    translate(0,0,-2);
  }

void draw(){
  background(250);
  
  if (barGraph != null){
    barGraph.init(table, currentRowIndex);
    barGraph.draw();
    // Check if new data point being hovered on in bar graph
    if (currentRowIndex != barGraph.getCurrentRowIndex()){
      currentRowIndex = barGraph.getCurrentRowIndex();
    }
  }

  if (lineGraph != null){
    lineGraph.init(table, currentRowIndex);
    lineGraph.draw();
    // Check if new data point being hovered on in line graph
    if (currentRowIndex != lineGraph.getCurrentRowIndex()){
      currentRowIndex = lineGraph.getCurrentRowIndex();
    }
  }
  
  if (scatterPlot != null){
    scatterPlot.init(table, currentRowIndex);
    scatterPlot.draw();
    // Check if new data point being hovered on in scatter plot graph
    if (currentRowIndex != scatterPlot.getCurrentRowIndex()){
      currentRowIndex = scatterPlot.getCurrentRowIndex();
    }
  }
  
  int k = 0;
  for (int i = 0; i < table.getColumnCount(); i++) {
    for (int j = 0; j < table.getColumnCount(); j++) {
      if (i == j) continue;
      ScatterPlot scatterPlotMatrix = setupScatterPlotMatrix(table, k, i, j);
       if (scatterPlotMatrix != null){
         scatterPlotMatrix.draw();
         k++;
         if (currentRowIndex != scatterPlotMatrix.getCurrentRowIndex()){
           currentRowIndex = scatterPlotMatrix.getCurrentRowIndex();
         }
       }
    }
  }
  
  if (pGraph != null){
    pGraph.init(table, currentRowIndex);
    pGraph.draw();
    // Check if new data point being hovered on in parallel graph
    if (currentRowIndex != pGraph.getCurrentRowIndex()){
      currentRowIndex = pGraph.getCurrentRowIndex();
    }
  }
  
  drawTooltip();
}