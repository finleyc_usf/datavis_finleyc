String filename;
ParallelGraph pGraph = null;
Table table = null;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

ParallelGraph setupParallelGraph(Table table){
   ParallelGraph pg = new ParallelGraph();
   pg.setPosition(200, 100, 400, 400);
   pg.init(table);
   return pg;
}

void setup(){
  size(800,600,P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
  pGraph = setupParallelGraph(table);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void draw(){
  background(250);
  textAlign(LEFT);
  text("Note: because Processing\ndoesn't have good click\nhandling, you must click\nthe swap buttons a few\ntimes before it works.", 10, 100);
  if (pGraph != null){
    pGraph.draw();
  }
}