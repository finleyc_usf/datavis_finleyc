class ParallelGraph extends Graph {
  
  int axisIntervalCount = 10;
  int hoveredRow;
  int columnCount;
  
  // Used to keep track of each column axis and its respective data min and max
  HashMap<Integer, FloatList> axisMap = new HashMap<Integer, FloatList>();
  
  // Used for visual->data mapping when swapping columns
  int[] columnOrder;
  
  void init(Table table){
    setTable(table);
    columnCount = table.getColumnCount();
    initColumnOrder();
    initAxisMap();
  }
  
  void initColumnOrder(){
    // Start column order for later remapping
    columnOrder = new int[columnCount];
    for (int i = 0; i < columnCount; i++) {
      columnOrder[i] = i;
    }
  }
  
  void initAxisMap() {
    // Map columns to their respective min and max values in table
    for (int i = 0; i < columnCount; i++){
      int mappedColumnIndex = columnOrder[i];
      // Get min/max of column, add to axis hash map (columnIndex: [min, max])
      float min = min(table.getFloatColumn(mappedColumnIndex));
      float max = max(table.getFloatColumn(mappedColumnIndex));
      FloatList range = new FloatList();
      range.append(min);
      range.append(max);
      this.axisMap.put(mappedColumnIndex, range);
    }
  }
  
  void drawVerticalAxis(int columnIndex) {
    int mappedColumnIndex = columnOrder[columnIndex];
    
    // Space out columns equally
    float hScale = this.w / (columnCount-1);
    float horizontalOffset = this.x0 + (columnIndex * hScale);
    
    float pixelMin = this.y0+this.h;
    float pixelMax = this.y0;
    
	  // Draw vertical axis line
    stroke(0);
    strokeWeight(2);
    line(horizontalOffset, pixelMax, horizontalOffset, pixelMin);

  	// Label at top
  	fill(0);
    textAlign(CENTER);
    textSize(12);
  	text(table.getColumnTitle(mappedColumnIndex), horizontalOffset, y0 - 20);

    // Arrow buttons at top
    if (columnIndex != 0) {
      // Left arrow if not left-most column
      String arrowLeft = "<-";
      fill(255);
      rect(horizontalOffset-35, y0-40, 25, -15);
      fill(0);
      text(arrowLeft, horizontalOffset-25, y0-44);
      
      // If user hovers over arrow button and clicks, swap columns in appropriate direction
      if (mouseX>horizontalOffset-35 && mouseX<horizontalOffset-10 &&
      mouseY>y0-40-15 && mouseY<y0-40){
        // Hover over left arrow button
        // Because I can't use mouseClicked here, the condition
        // will be met again and again so stuttering will appear
        if (mousePressed) {
          swapAxis(columnIndex, -1);
        }
      }
    }
    
    if (columnIndex != columnCount-1) {
      // Right arrow if not right-most column
      String arrowRight = "->";
      fill(255);
      rect(horizontalOffset, y0-40, 25, -15);
      fill(0);
      text(arrowRight, horizontalOffset+15, y0-44);
      
      if (mouseX>horizontalOffset && mouseX<horizontalOffset+25 &&
      mouseY>y0-40-15 && mouseY<y0-40){
        // Hover over left arrow button
        if (mousePressed) {
          swapAxis(columnIndex, 1);
        }
      }
    }
    
    // Draw and label intervals
    for (int i = 0; i <= axisIntervalCount; i++) {
      // Get min/max from axisMap that was set initially
      FloatList currentColumnMinMax = axisMap.get(mappedColumnIndex);
      float currentColumnMin = currentColumnMinMax.get(0);
      float currentColumnMax = currentColumnMinMax.get(1);
      float label = map(i, 0, axisIntervalCount, currentColumnMin, currentColumnMax);
      String labelText = String.format("%.1f", label);
      float yPos = map(i, 0, axisIntervalCount, pixelMin, pixelMax);
      line(horizontalOffset-2, yPos, horizontalOffset+2, yPos);
      textAlign(RIGHT);
      textSize(10);
      text(labelText, horizontalOffset-4, yPos+2); 
    }
  }
  
  void swapAxis(int columnIndex, int direction){
    // Swap axis with axis to left or right of it
    if (direction == -1) {
      // Swap left
      int mappedColumnCurrent = columnOrder[columnIndex];
      int mappedColumnLeft = columnOrder[columnIndex-1];
      columnOrder[columnIndex-1] = mappedColumnCurrent;
      columnOrder[columnIndex] = mappedColumnLeft;
    }
    else {
      // Swap right
      int mappedColumnCurrent = columnOrder[columnIndex];
      int mappedColumnRight = columnOrder[columnIndex+1];
      columnOrder[columnIndex+1] = mappedColumnCurrent;
      columnOrder[columnIndex] = mappedColumnRight;
    }
    
  }
  
  void drawDataLines() {
    
    float pixelMin = this.y0+this.h;
    float pixelMax = this.y0;
    int blue = 0;
    int red = 255;
    int rowCount = table.getRowCount();
    float colorScale = 255.0 / rowCount;
    float hScale = this.w / (columnCount-1);
    
    for (int i = 0; i < table.getRowCount(); i++) {
      
      // Gradual color change for each data point
      stroke(red, 100, blue);
      red -= colorScale;
      blue += colorScale;

      TableRow row = table.getRow(i);
      for (int j = 0; j < columnCount-1; j++) {
        
        // Get mapped data columns for current and next visual columns
        int j1 = columnOrder[j];
        int j2 = columnOrder[j+1];
        
        // Get min/max of current column from hashmap
        FloatList currentColumnMinMax = axisMap.get(j1);
        float currentColumnMin = currentColumnMinMax.get(0);
        float currentColumnMax = currentColumnMinMax.get(1);
        
        // Calculate x and y
        float currentHorizontalOffset = this.x0 + (j * hScale); // x1
        float currentColumnData = row.getFloat(j1); // y1
        float pixelMappedCurrentColumnData = map(currentColumnData, currentColumnMin, currentColumnMax, pixelMin, pixelMax); // y1 pixel
        
        // Get min/max of next column from hashmap
        FloatList nextColumnMinMax = axisMap.get(j2);
        float nextColumnMin = nextColumnMinMax.get(0);
        float nextColumnMax = nextColumnMinMax.get(1);
        
        // Calculate x and y
        float nextHorizontalOffset = this.x0 + ((j+1) * hScale); // x2
        float nextColumnData = row.getFloat(j2); // y2
        float pixelMappedNextColumnData = map(nextColumnData, nextColumnMin, nextColumnMax, pixelMin, pixelMax); // y2 pixel
        
        if (dist(mouseX,mouseY,currentHorizontalOffset,pixelMappedCurrentColumnData)<3){
          drawTooltip(i, mouseX, mouseY);
          hoveredRow = i;
        }
        
        else if (dist(mouseX,mouseY,nextHorizontalOffset,pixelMappedNextColumnData) < 2){
          drawTooltip(i, mouseX, mouseY);
          hoveredRow = i;
        }
        
        // Draw line connecting axes
        if (i == hoveredRow) {
          strokeWeight(3);
        }
        line(currentHorizontalOffset, pixelMappedCurrentColumnData, nextHorizontalOffset, pixelMappedNextColumnData);
        strokeWeight(1);
      }
      
    }
    stroke(0);
  }
  
  
  
  void drawTooltip(int rowIndex, float posX, float posY) {
    // Draw white rectangle with black text showing data attributes at cursor
    int charWidth = 8;
    int charHeight = 17;
    
    StringDict rowValues = new StringDict();
    int maxLength = 0;
    // Get row key/value pairs, calculate longest string for rectangle width
    for (int i = 0; i < columnCount; i++) {
      int i1 = columnOrder[i];
      String columnTitle = table.getColumnTitle(i1); 
      String columnValue = String.valueOf(table.getRow(rowIndex).getFloat(i1));
      rowValues.set(columnTitle, columnValue);
      if (columnTitle.length() + 2 + columnValue.length() > maxLength) {
        maxLength = columnTitle.length() + 2 + columnValue.length();
      } 
    }
    
    // Calculate width and height, draw box and text
    int rectLength = maxLength * charWidth;
    int rectHeight = columnCount * charHeight;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    
    fill(0);
    textAlign(LEFT);
    String[] keys = rowValues.keyArray();
    for (int i = 0; i < keys.length; i++) {
      String s = keys[i] + ": " + rowValues.get(keys[i]);
      text(s, posX+5, posY - (i*charHeight) - 3, 5);
    }
    translate(0,0,-2);    
  }
  
  void drawVerticalAxes() {
    // For each column, draw labeled axis
    for (int i = 0; i < columnCount; i++) {
      drawVerticalAxis(i);
    }
  }
  
  void draw() {
    drawFrame();
    drawVerticalAxes();
    drawDataLines();
  }
}