class Histogram extends XYGraph {

	int binCount;
	int[] hist;
  
	int columnIndex;
	int currentRowIndex;
	float rectWidth;
	String columnTitle;
	int maxAmount;
	float mean;
	float stdev;

	void init(Table table, int columnIndex) {
  	maxAmount = 0;
		binCount = 30;
		hist = new int[binCount];
		setTable(table);
		this.columnIndex = columnIndex;
		setScalesOneDimensional(columnIndex);
	}

	void setScalesOneDimensional(int columnIndex){
		this.xColumnTitle = table.getRow(0).getColumnTitle(columnIndex);
  	this.yColumnTitle = "";

		xPixelStart = x0;
		xPixelStop = x0+w;
		
  	// X-axis: actual values
		xDataStart = min(table.getFloatColumn(columnIndex));
		xDataStop = max(table.getFloatColumn(columnIndex));

		if (xDataStop < defaultAxisIntervalCount)
		  this.xAxisIntervalCount = (int) xDataStop;
		else
		  this.xAxisIntervalCount = defaultAxisIntervalCount;
    if (xDataStop < binCount)
      binCount = (int) xDataStop;
		
		yPixelStart = y0 + h;
		yPixelStop = y0;
		
    // Y-axis: count
		yDataStart = 0;
		yDataStop = table.getRowCount();
		
		if (yDataStop < defaultAxisIntervalCount)
		  this.yAxisIntervalCount = (int) yDataStop;
		else
		  this.yAxisIntervalCount = defaultAxisIntervalCount;
	}

	ArrayList<Float> getValues(float[] arr) {
    ArrayList<Float> processed = new ArrayList<Float>();

    for (float val : arr) {
      if (Double.isNaN(val))
        continue;
      processed.add(val);
    }
    return processed;
  }

	void generateHistogram() {
		ArrayList<Float> arr = getValues(table.getFloatColumn(this.columnIndex));
		float sum = 0;
		float meanSquaredSum = 0;
		int totalRows = arr.size();
		for (float val : arr) {
      sum += val;
			int bin = (int) map(val, xDataStart, xDataStop, 0, binCount-1);
			hist[bin] += 1;
		}

		mean = sum / totalRows;

		for (float val : arr) {
			meanSquaredSum += pow((val - mean), 2);
		}
		stdev = sqrt(meanSquaredSum / totalRows);

		this.rectWidth = w / binCount;

		maxAmount = max(hist);
    this.yDataStop = maxAmount;
	}

	void drawDataPoints() {
		for (int i = 0; i < binCount; i++) {
			fill(0,100,150);
			int val = hist[i];
			float xPos = x0+(i*rectWidth);
			float rectHeight = map(val, yDataStart, yDataStop, yPixelStart, yPixelStop);

			int binInverted = (int) map(i, 0, binCount-1, xDataStart, xDataStop);
			int binInvertedNext = (int) map(i+1, 0, binCount-1, xDataStart, xDataStop);

			String currentRange = "[" + binInverted + ", " + binInvertedNext + "]";

			// Hover
			if (mouseX > xPos && mouseX < xPos+rectWidth && 
			mouseY < this.y0+h && mouseY > (this.y0+h)-(this.y0+h-rectHeight)) {
			  drawTooltip(mouseX, mouseY, currentRange, val);
			  fill(255,0,0);
			}

			rect(xPos, y0+h, rectWidth, -(y0+h-rectHeight));
		}
	}

	void drawTooltip(float posX, float posY, String range, float amount) {
		// Draw rectangle containing key value pairs for graph data
		int rectHeight = 35;
		int charWidth = 8;
		String topString = "Range: " + range;
		String bottomString = "Amount: " + amount;
		int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
		String s = topString + "\n" + bottomString;
		fill(255);
		stroke(0);
		strokeWeight(1);
		translate(0,0,2);
		rect(posX, posY, rectLength, -rectHeight);
		fill(0);
		stroke(200);
		textAlign(LEFT);
		text(s,posX+5,posY-rectHeight+15, 5);
		translate(0,0,-2);
	}
	
	void drawInfo() {
		fill(50);
		textAlign(CENTER);
		text("Mean: " + mean, x0+(w/2), yPixelStart+40);
		text("Std. Dev.: " + stdev, x0+(w/2), yPixelStart+50);
	}

	void drawTitle(){
    textAlign(CENTER);
    fill(0);
    text(xColumnTitle + " Histogram", x0+(w/2), y0-15);
  }

	void draw() {
		drawFrame();
		drawTitle();
		generateHistogram();
		drawVerticalAxis();
		drawHorizontalAxis();
		drawDataPoints();
		drawInfo();
	}
}