abstract class XYGraph extends Graph{
  
  // Scales are in the form of spacing in-between data points and intervals for horizontal and vertical, respectively
  float horizontalScale, verticalScale, maxXValue, maxYValue, yDataScale, xDataScale, yAxisIntervalCount, xAxisIntervalCount, yScaleMultiplier, xScaleMultiplier;
  String yColumnTitle, xColumnTitle;
  float defaultAxisIntervalCount = 4.0;
  
  int xColumnIndex;
  int yColumnIndex;
  
  float minXValue, minYValue;
  float xDataStart, xDataStop, xPixelStart, xPixelStop, yDataStart, yDataStop, yPixelStart, yPixelStop;
  
  int currentRowIndex;
  
  void init(Table table, int currentRowIndex){
    setTable(table);
    xColumnIndex = 0;
    yColumnIndex = 1;
    this.currentRowIndex = currentRowIndex;
    setScales(xColumnIndex, yColumnIndex);
  }
  
  void initMatrix(Table table, int xColumnIndex, int yColumnIndex, int currentRowIndex){
    setTable(table);
    this.currentRowIndex = currentRowIndex;
    setScales(xColumnIndex, yColumnIndex);
  }
  
  void setScales(int xColNumber, int yColNumber){
    
    this.xColumnTitle = table.getRow(0).getColumnTitle(xColNumber);
    this.yColumnTitle = table.getRow(0).getColumnTitle(yColNumber);
    
    xPixelStart = x0;
    xPixelStop = x0+w;
    
    //xDataStart = findMinColumnValue(xColumnTitle);
    xDataStart = min(table.getFloatColumn(xColNumber));
    xDataStop = max(table.getFloatColumn(xColNumber));
    
    if (xDataStop < defaultAxisIntervalCount)
      this.xAxisIntervalCount = (int) xDataStop;
    else
      this.xAxisIntervalCount = defaultAxisIntervalCount;

    yPixelStart = y0 + h;
    yPixelStop = y0;
    
    yDataStart = min(table.getFloatColumn(yColNumber));
    yDataStop = max(table.getFloatColumn(yColNumber));
    
    if (yDataStop < defaultAxisIntervalCount)
      this.yAxisIntervalCount = (int) yDataStop;
    else
      this.yAxisIntervalCount = defaultAxisIntervalCount;
    
  }
  
  void drawVerticalAxis(){
    // Label axis
    textAlign(RIGHT);
    text(yColumnTitle, x0-30, y0+(h/2));
    for (int j = 0; j <= yAxisIntervalCount; j++){

      // Calculate visual interval for ticks/lines
      float intervalY = map(j, 0, yAxisIntervalCount, yPixelStart, yPixelStop);
      
      // Label visual interval with actual values scaled to match graph's pixel height
      int label = (int) map(j, 0, yAxisIntervalCount, yDataStart, yDataStop);
      fill(50);
      textAlign(RIGHT);
      text(label, x0-5, intervalY);
      
      // Draw interval lines across graph
      stroke(200);
      strokeWeight(1);
      line(this.x0, intervalY, x0+w, intervalY);
    }
  }
  
  void drawHorizontalAxis(){
    textAlign(CENTER);
    text(this.xColumnTitle, x0+(w/2), (y0+h)+25);
    for (int i = 1; i <= xAxisIntervalCount; i++) { 
      // Calculate visual interval along x-axis
      float intervalX = map(i, 1, xAxisIntervalCount, xPixelStart, xPixelStop);
    
      // Label visual interval with actual values scaled to match graph's pixel width
      int label = (int) map(i, 1, xAxisIntervalCount, xDataStart, xDataStop);
      fill(50);
      textAlign(CENTER);
      text(label, intervalX, yPixelStart+15);
    }
  }
  
  void drawTitle(){
    textAlign(CENTER);
    fill(0);
    text(xColumnTitle + " vs. " + yColumnTitle, x0+(w/2), y0-15);
  }
  
  void drawTooltip(float posX, float posY, float dataX, float dataY) {
    // Draw rectangle containing key value pairs for graph data
    int rectHeight = 35;
    int charWidth = 8;
    String topString = this.xColumnTitle + ": " + dataX;
    String bottomString = this.yColumnTitle + ": " + dataY;
    int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
    String s = topString + "\n" + bottomString;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    fill(0);
    textAlign(LEFT);
    text(s,posX+5,posY-rectHeight+15, 5);
    translate(0,0,-2);
  }
  
  void updateCurrentRowIndex(int i) {
    // Update synced data point for global use
    currentRowIndex = i;
  }
  
  int getCurrentRowIndex() {
    // Used in main for syncing current data point
    return currentRowIndex;
  }
  
  abstract void draw();
}