class Corrgram extends Graph {

  float blockSize;
  Boolean is_spearmans;
  
  void init(Table table, Boolean is_spearmans) {
    setTable(table);
    blockSize = w / table.getColumnCount();
    this.is_spearmans = is_spearmans;
  }

  void drawCorrelation(int x, int y, float c) {
    float xPos = x0 + (x*blockSize);
    float yPos = y0 + (y*blockSize);
    if (c < 0)
      fill(255*c, 50, 50);
    else
      fill(50, 255*c, 50);
    rect(xPos, yPos, blockSize, blockSize);
    if (mouseX > xPos && mouseX < xPos+blockSize && 
      mouseY < yPos + blockSize && mouseY > yPos) {
        drawTooltip(mouseX, mouseY, x, y, c);
      }
  }

  ArrayList<Float> rankArray(ArrayList<Float> arr, int n) {
    ArrayList<Float> rankedArray = new ArrayList<Float>();

    for (int i = 0; i < n; i++) {
        int count = 0;
        for (int j = 0; j < n; j++) {
            if (arr.get(j) < arr.get(i))
                count++;
        }
        rankedArray.add((float) count + 1);
    }
    return rankedArray;
  }

  ArrayList<Float> getValues(float[] arr) {
    ArrayList<Float> processed = new ArrayList<Float>();

    for (float val : arr) {
      if (Double.isNaN(val))
        continue;
      processed.add(val);
    }
    if (is_spearmans)
      processed = rankArray(processed, processed.size());
    return processed;
  }
  
  float calculateCorrelation(int x, int y, int totalRows){
    
    // X
    ArrayList<Float> arrX = getValues(table.getFloatColumn(x));
    float sumX = 0;
    for (float val: arrX) {
      sumX += val;
    }
    float meanX = sumX / totalRows;

    float meanSquaredSumX = 0;
    for (float val : arrX) {
      meanSquaredSumX += pow((val - meanX), 2);
    }
    float stdevX = sqrt(meanSquaredSumX / totalRows);

    // Y

    ArrayList<Float> arrY = getValues(table.getFloatColumn(y));
    float sumY = 0;
    for (float val: arrY) {
      sumY += val;
    }
    float meanY = sumY / totalRows;

    float meanSquaredSumY = 0;
    for (float val : arrY) {
      meanSquaredSumY += pow((val - meanY), 2);
    }
    float stdevY = sqrt(meanSquaredSumY / totalRows);

    // Covariance

    float covar_sum = 0;

    for (int k = 0; k < arrX.size(); k++) {
      covar_sum += ((arrX.get(k) - meanX) * (arrY.get(k) - meanY));
    }
    float covariance = covar_sum / totalRows;

    // Correlation

    float correlationCoefficient = covariance / (stdevX*stdevY);
    return correlationCoefficient;
  }

  void calculateCorrelations() {
    int totalRows = table.getRowCount();
    for (int x = 0; x < table.getColumnCount(); x++) {
      for (int y = x+1; y < table.getColumnCount(); y++) {
        float correlationCoefficient = calculateCorrelation(x, y, totalRows);
        drawCorrelation(x, y, correlationCoefficient);
      }
    }
  }
  
  void drawTitle(){
    textAlign(CENTER);
    fill(0);
    if (is_spearmans)
      text("Spearman's Rank Correlation Graph", x0+(w/2), y0-15);
    else
      text("Pearson Correlation Graph", x0+(w/2), y0-15);
  }

  void drawTooltip(float posX, float posY, int x, int y, float c) {
    // Draw rectangle containing key value pairs for graph data
    int rectHeight = 35;
    int charWidth = 6;
    String topString = table.getRow(0).getColumnTitle(x) + " vs. " + table.getRow(0).getColumnTitle(y);
    String bottomString;
    if (is_spearmans)
      bottomString = "Spearman's Rank Correlation Coefficient: " + c;
    else
      bottomString = "Pearson Correlation Coefficient: " + c;
    int rectLength = Math.max(topString.length(), bottomString.length()) * charWidth;
    String s = topString + "\n" + bottomString;
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(posX, posY, rectLength, -rectHeight);
    fill(0);
    textAlign(LEFT);
    text(s,posX+5,posY-rectHeight+12, 5);
    translate(0,0,-2);
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    calculateCorrelations();
  }
}