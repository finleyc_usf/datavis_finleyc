String filename;
BarGraph barGraph = null;
//LineGraph lineGraph = null;
ScatterPlot scatterPlot = null;
//ScatterPlot scatterPlotMatrix = null;
ParallelGraph pGraph = null;
Corrgram cgramPearson = null;
Corrgram cgramSpearman = null;
Table table = null;
int currentRowIndex = 0;

void fileSelected(File selection) {
  // Get data file
  if (selection == null) {
    filename = "";
  } else {
    filename = selection.getAbsolutePath();
  }
}

Table processCSV(String filename) {
  // Get table object from supplied data
  Table table = loadTable(filename, "header");
  return table;
}

BarGraph setupBarGraph(Table table){
     BarGraph bg = new BarGraph();
     bg.setPosition(70, 50, 200, 200);
     bg.init(table, currentRowIndex);
     return bg;
}


ScatterPlot setupScatterPlot(Table table){
   ScatterPlot sp = new ScatterPlot();
   sp.setPosition(370, 50, 200, 200);
   sp.init(table, currentRowIndex);
   return sp;
}


ParallelGraph setupParallelGraph(Table table){
   ParallelGraph pg = new ParallelGraph();
   pg.setPosition(630, 50, 400, 200);
   pg.init(table, currentRowIndex);
   return pg;
}

Histogram setupHistogram(Table table, int columnIndex) {
  Histogram hist = new Histogram();
  hist.setPosition(40 + (200*columnIndex), 320, 150, 150);
  hist.init(table, columnIndex);
  return hist;
}

Corrgram setupCorrgramPearson(Table table) {
  Corrgram cgram = new Corrgram();
  cgram.setPosition(40, 570, 200, 200);
  cgram.init(table, false);
  return cgram;
}

Corrgram setupCorrgramSpearman(Table table) {
  Corrgram cgram = new Corrgram();
  cgram.setPosition(300, 570, 200, 200);
  cgram.init(table, true);
  return cgram;
}

void setup(){
  // Couldn't fit everything neatly into 1200x800
  size(1200,800,P3D);
  selectInput("Select a file to process:", "fileSelected");
  waitForFilename();
  table = processCSV(filename);
  barGraph = setupBarGraph(table);
  scatterPlot = setupScatterPlot(table);
  pGraph = setupParallelGraph(table);
  cgramPearson = setupCorrgramPearson(table);
  cgramSpearman = setupCorrgramSpearman(table);
}

void waitForFilename(){
  // Halt processing until file selected, otherwise null pointer
  while (null == filename){
    delay(200);
  }
}

void drawTooltip() {
    // Tooltip, but for synced data point, off to side of all graphs
    int rectHeight = 20 * table.getColumnCount();
    int charWidth = 7;
    String text = "";
    int rectLength = 0;
    TableRow currentRow = table.getRow(currentRowIndex);
    for (int i = 0; i < table.getColumnCount(); i++){
      String label = currentRow.getColumnTitle(i);
      String data = currentRow.getString(i);
      String line = label + ": " + data + "\n";
      if ((line.length() * charWidth) > rectLength) {
        rectLength = line.length() * charWidth;
      }
      text += line;
    }
    fill(255);
    stroke(0);
    strokeWeight(1);
    translate(0,0,2);
    rect(1050, 50, rectLength, rectHeight);
    fill(0);
    textAlign(LEFT);
    text(text, 1055, 65);
    translate(0,0,-2);
  }

void draw(){
  background(250);
  
  if (barGraph != null){
    barGraph.init(table, currentRowIndex);
    barGraph.draw();
    // Check if new data point being hovered on in bar graph
    if (currentRowIndex != barGraph.getCurrentRowIndex()){
      currentRowIndex = barGraph.getCurrentRowIndex();
    }
  }

  
  if (scatterPlot != null){
    scatterPlot.init(table, currentRowIndex);
    scatterPlot.draw();
    // Check if new data point being hovered on in scatter plot graph
    if (currentRowIndex != scatterPlot.getCurrentRowIndex()){
      currentRowIndex = scatterPlot.getCurrentRowIndex();
    }
  }
  
  
  if (pGraph != null){
    pGraph.init(table, currentRowIndex);
    pGraph.draw();
    // Check if new data point being hovered on in parallel graph
    if (currentRowIndex != pGraph.getCurrentRowIndex()){
      currentRowIndex = pGraph.getCurrentRowIndex();
    }
  }

  for (int i = 0; i < table.getColumnCount(); i++) {
    Histogram hist = setupHistogram(table, i);
    if (hist != null){
      hist.draw();
    }
  }

  if (cgramPearson != null) {
    cgramPearson.draw();
  }
  if (cgramSpearman != null) {
    cgramSpearman.draw();
  }
  
  drawTooltip();
}