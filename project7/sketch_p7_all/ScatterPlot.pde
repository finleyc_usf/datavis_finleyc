class ScatterPlot extends XYGraph {

  void drawDataPoints() {
    int dotSize = 5;
    for (int i=0; i < table.getRowCount(); i++) {
      TableRow row = table.getRow(i);
      float dataX = row.getFloat(this.xColumnTitle);
      float dataY = row.getFloat(this.yColumnTitle);
      if (Double.isNaN(dataX) || Double.isNaN(dataY))
        continue;
      float posX = map(dataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
      float posY = map(dataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
      fill(0,0,0);
      
      if (currentRowIndex == i){
        // If on synced data point, enlarge dot size and make red
        fill(255,0,0);
        dotSize = 10;
      }

      if(dist(mouseX,mouseY,posX,posY)<3){
        // If user hovering over/close to data point, show tooltip
        drawTooltip(posX, posY, dataX, dataY);
        // Update synced data point for global use
        updateCurrentRowIndex(i);
        fill(255,0,0);
        dotSize = 10;
      }
      
      noStroke();
      translate(0,0,1);
      ellipse(posX, posY, dotSize, dotSize);
      translate(0,0,-1);
      dotSize = 5;
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
}