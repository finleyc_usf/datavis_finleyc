class LineGraph extends XYGraph {
    
  void drawDataPoints() {
    int dotSize = 5;
    for (int i = 0; i < table.getRowCount(); i++){
      float dataX = table.getRow(i).getFloat(this.xColumnTitle);
      float dataY = table.getRow(i).getFloat(this.yColumnTitle);

      float posX = map(dataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
      float posY = map(dataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
      
      fill(0,0,0);
      
      if (currentRowIndex == i){
        // If on synced data point, enlarge dot size and make red
        dotSize = 10;
        fill(255,0,0);
      }
      
      // Hover
      if(dist(mouseX,mouseY,posX,posY)<3){
          drawTooltip(posX, posY, dataX, dataY);
          // Update synced data point for global use
          updateCurrentRowIndex(i);
          fill(255,0,0);
          dotSize = 10;
      }
      
      noStroke();
      translate(0,0,1);
      ellipse(posX, posY, dotSize, dotSize);
      translate(0,0,-1);
      dotSize = 5;
      
      // Draw lines from current to next data point (if not at last data point)
      if (i+1 != table.getRowCount()) {
        float nextDataX = table.getRow(i+1).getFloat(this.xColumnTitle);
        float nextDataY = table.getRow(i+1).getFloat(this.yColumnTitle);
        //float nextPosX = x0 + (nextDataX * xScaleMultiplier);
        float nextPosX = map(nextDataX, xDataStart, xDataStop, xPixelStart, xPixelStop);
        //float nextPosY = y0+this.h + (nextDataY * -yScaleMultiplier);
        float nextPosY = map(nextDataY, yDataStart, yDataStop, yPixelStart, yPixelStop);
        stroke(0, 100, 150);
        line(posX, posY, nextPosX, nextPosY);
      }
      fill(50);
    }
  }
  
  void draw(){
    drawFrame();
    drawTitle();
    drawVerticalAxis();
    drawHorizontalAxis();
    drawDataPoints();
  }
  
}