The sketch uses the 3D renderer which can be buggy.
	The file dialog may appear behind the sketch window.
	The sketch window may launch in the background where it can't be focused.
		If this happens, restart the sketch.